import axios from "axios";

const _axios = axios.create();
const _baseUrl = "http://localhost:8080/api/v1/"

class CalculatorService {
    getProductList = () => {
        return _axios.get(_baseUrl + "product/");
    }

    calculateTotal = (productCode: string, count: number) => {
        return _axios.get(_baseUrl + "calculate/", {params: {"productCode": productCode, "count": count}});
    }
}

const calculatorService = new CalculatorService();
export default calculatorService;
