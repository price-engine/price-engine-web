export interface CalculatorResponse {
    productCode: string,
    productName: string,
    noOfCartons: number,
    cartonPrice: number,
    manualPacking: number,
    individualPrice: number,
    discount: number,
    packingCharges: number,
    total: number
}
