import React from "react";
import {Button, Card, Col, Form, FormInstance, InputNumber, Row, Select, Space, Table, Typography} from "antd";
import {CalculatorResponse} from "../model/CalculatorResponse";
import {ListItem} from "../model/ListItem";
import calculatorService from "../service/CalculatorService";

const {Option} = Select;
const {Text} = Typography;

interface SummaryCompProps {
    // summary: Array<CalculatorResponse>
}

interface SummaryCompState {
    summaryList: Array<CalculatorResponse>;
    productList: Array<ListItem>;
}

export class SummaryComp extends React.Component<SummaryCompProps, SummaryCompState> {

    constructor(props: Readonly<any> | any) {
        super(props);
        this.state = {
            summaryList: [],
            productList: []
        }
    }

    myRef: any = React.createRef<FormInstance>();
    columns: any = [
        {
            title: "Product Code",
            dataIndex: "productCode",
            key: "productCode",
            align: "center"
        },
        {
            title: "Product name",
            dataIndex: "productName",
            key: "productName",
            align: "center"
        },
        {
            title: "Item Price",
            dataIndex: "individualPrice",
            key: "individualPrice",
            align: "center"
        },
        {
            title: "Discount",
            dataIndex: "discount",
            key: "discount",
            align: "center"
        },
        {
            title: "Packing Charges",
            dataIndex: "packingCharges",
            key: "packingCharges",
            align: "center"
        },
        {
            title: "Total",
            dataIndex: "total",
            key: "total",
            align: "center"
        }
    ]

    componentDidMount() {
        this.loadProducts();
    }

    loadProducts = () => {
        calculatorService.getProductList().then(res=> {
            this.setState({
                productList: res.data
            });
        });
    }

    onFinish = async (values: any) => {
        const calculateResponse = await calculatorService.calculateTotal(values.product, values.count);
        let arr = this.state.summaryList;
        arr.push(calculateResponse.data);
        // setSummary(arr);
        // setSummary([calculateResponse.data]);
        this.setState({
            summaryList: [calculateResponse.data]
        })
    }

    render() {
        return (
            <Row gutter={20}>
                <Col span={100}>
                    <Card title={"Price Calculator"}>
                        <Card>
                            <Form
                                ref={this.myRef}
                                name="basic"
                                labelCol={{span: 9}}
                                wrapperCol={{span: 5}}
                                onFinish={this.onFinish}
                                // onFinishFailed={onFinishFailed}
                                autoComplete="off">
                                <Form.Item
                                    label={"Product"}
                                    name={"product"}
                                    rules={[{required: true, message: "Please select a Product!"}]}
                                >
                                    <Select
                                        showSearch
                                        style={{width: "100%"}}
                                        placeholder="Select a Product"
                                        allowClear
                                        optionFilterProp="children"
                                    >
                                        {
                                            this.state.productList.map((product: ListItem) => {
                                                return <Option value={product.value}>{product.label}</Option>
                                            })
                                        }
                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label={"Count"}
                                    name={"count"}
                                    rules={[{required: true, message: 'Please select item count'}, {
                                        type: 'number',
                                        min: 0,
                                        max: 2000
                                    }]}
                                >
                                    <InputNumber/>
                                </Form.Item>
                                <Form.Item wrapperCol={{offset: 10, span: 10}}>
                                    <Button type="default" onClick={() => {
                                        this.myRef.current.resetFields();
                                    }}>
                                        Clear
                                    </Button>
                                    <Space/>
                                    <Button type="primary" htmlType="submit">
                                        Calculate
                                    </Button>
                                </Form.Item>
                            </Form>
                        </Card>
                        <Card>
                            <Table
                                columns={this.columns}
                                dataSource={this.state.summaryList}
                                size="small"
                                scroll={{y: 400}}
                            />
                        </Card>
                    </Card>
                </Col>
            </Row>
        );
    }
}
