import React, {useEffect, useState} from 'react';
import './App.css';
import "antd/dist/antd.css";
import {Button, Card, Col, Form, InputNumber, Row, Select, Space, Table, Typography} from "antd";
import {ListItem} from "./model/ListItem";
import calculatorService from "./service/CalculatorService";
import {CalculatorResponse} from "./model/CalculatorResponse";
import {SummaryComp} from "./component/SummaryComp";

const {Option} = Select;
const {Text} = Typography;

function App() {
    const [productList, setProductList] = useState<ListItem[]>([]);
    const [summary, setSummary] = useState<CalculatorResponse[]>([]);
    const myRef: any = React.createRef();

    useEffect(() => {
        const loadProducts = async () => {
            const products = await fetchProducts();
            setProductList(products);
        }
        loadProducts()
    }, []);

    const fetchProducts = async () => {
        const axiosResponse = await calculatorService.getProductList();
        return axiosResponse.data;
    }

    const onFinish = async (values: any) => {
        const calculateResponse = await calculatorService.calculateTotal(values.product, values.count);
        // let arr = summary;
        // arr.push(calculateResponse.data);
        // setSummary(arr);
        setSummary([calculateResponse.data]);
    }

    const columns: any = [
        {
            title: "Product Code",
            dataIndex: "productCode",
            key: "productCode",
            align: "center"
        },
        {
            title: "Product name",
            dataIndex: "productName",
            key: "productName",
            align: "center"
        },
        {
            title: "Item Price",
            dataIndex: "individualPrice",
            key: "individualPrice",
            align: "center"
        },
        {
            title: "Discount",
            dataIndex: "discount",
            key: "discount",
            align: "center"
        },
        {
            title: "Packing Charges",
            dataIndex: "packingCharges",
            key: "packingCharges",
            align: "center"
        },
        {
            title: "Total",
            dataIndex: "total",
            key: "total",
            align: "center"
        }
    ]

    return (
        // <SummaryComp/>
        <Row gutter={20}>
            <Col span={100}>
                <Card title={"Price Calculator"}>
                    <Card>
                        <Form
                            ref={myRef}
                            name="basic"
                            labelCol={{span: 9}}
                            wrapperCol={{span: 5}}
                            onFinish={onFinish}
                            // onFinishFailed={onFinishFailed}
                            autoComplete="off">
                            <Form.Item
                                label={"Product"}
                                name={"product"}
                                rules={[{required: true, message: "Please select a Product!"}]}
                            >
                                <Select
                                    showSearch
                                    style={{width: "100%"}}
                                    placeholder="Select a Product"
                                    allowClear
                                    optionFilterProp="children"
                                >
                                    {
                                        productList.map((product: ListItem) => {
                                            return <Option value={product.value}>{product.label}</Option>
                                        })
                                    }
                                </Select>
                            </Form.Item>
                            <Form.Item
                                label={"Count"}
                                name={"count"}
                                rules={[{required: true, message: 'Please select item count'}, {
                                    type: 'number',
                                    min: 0,
                                    max: 2000
                                }]}
                            >
                                <InputNumber/>
                            </Form.Item>
                            <Form.Item wrapperCol={{offset: 10, span: 10}}>
                                <Button type="default" onClick={() => {
                                    myRef.current.resetFields();
                                    setSummary([]);
                                }}>
                                    Clear
                                </Button>
                                <Space/>
                                <Button type="primary" htmlType="submit">
                                    Calculate
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                    <Card>
                        <Table
                            columns={columns}
                            dataSource={summary}
                            size="small"
                            scroll={{y: 400}}
                        />
                    </Card>
                </Card>
            </Col>
        </Row>
    );
}

export default App;
